# Guide for Team Members

Here are some resources for new software developers and Machine Learning engineers

## Workflow Patterns
You can use the conda environment manager instead of virtualenvwrapper in this [tutorial](https://github.com/FilippoBovo/production-data-science)

## Code patterns

### quantified code
This is a long list of patterns and antipatterns that I like to look at whenever I'm trying to decide between a couple different ways to do things. It helps to have a problem on mind that you're trying to solve. Otherwise this list is overwhelming:
https://docs.quantifiedcode.com/python-anti-patterns/
I want to try the quantifiedcode tool one day.

### Rosetta code
When I'm trying to solve a hard algorithms problem I'll check http://Rosettacode.org

### cs design patterns
my computer science friends swear by this book but the terminology is a bit jargony and cryptic:
https://www.journaldev.com/31902/gangs-of-four-gof-design-patterns

## code challenges/practice

###  Codility.com
Impressive, well thought out challenges and practice problems.  The best companies use this site to test applicants. It's similar to leetcode , only a bit smarter.

### project Euler
If you like math, you could get lost in this:
https://projecteuler.net/

## AI
This is by far the best book out there on algorithms for playing games, graph search, and real world AI problems: _artificial intelligence a modern approach_ by Norvig and Russell . The python code is here: https://github.com/aimacode/aima-python

### Norvig
Any videos or code examples by Norvig are brilliant, like this spell checker he built for Google: https://norvig.com/spell-correct.html

## thinking tools
### intuition pumps
These "neck-top" apps by Daniel Bennett have been running in the background in my brain for most of my life. I refer to this book to try to explain them to friends: _Intuition Pumps_ here's a video with some of them: https://m.youtube.com/watch?feature=youtu.be&t=3403&v=EJsD-3jtXz0
Attachments area
Preview YouTube video Daniel Dennett on Tools To Transform Our Thinking

