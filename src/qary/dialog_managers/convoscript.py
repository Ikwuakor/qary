""" Processes *.dialog.v2.yml files to define Finite State Machine rules """
# from qary.etl.dialog import TurnsPreparation, compose_statement, load_dialog_turns
from qary.etl.utils import squash_wikititle, normalize_wikititle, slugify, simple_tokenize
from qary.spacy_language_model import tokenize


def get_nxt_cndn_match_mthd_dict(nxt_cndn):
    """Creates a dict with the match_method keyword being the key and a list of next_states which use
    that keyword as a value. This is needed because there is a priority in which the match_method
    keywords are handled

    Args:
        nxt_cndn (dict): dict with the intent being the key and the value being another dict
            with a key value pair for the next state and the 'match_method' for that intent

    Returns:
        object: A dictionary which looks like :
                { match_method1: [intent1: next_state1], } where the list is a list of all the
                next_conditions that use that match method

    """
    nxt_cndn_match_mthd_dict = {
        'EXACT': [],
        'LOWER': [],
        'CASE_SENSITIVE_KEYWORD': [],
        'KEYWORD': [],
        'NORMALIZE': [],
        None: [],
    }
    for intent, nxt_state_dict in nxt_cndn.items():
        match_method = nxt_state_dict['match_method']
        next_state = nxt_state_dict['next_state']
        nxt_cndn_match_mthd_dict[match_method].append((intent, next_state))
    return nxt_cndn_match_mthd_dict


def check_match(self, statement, next_state_option, match_condition):
    """ Return the next

    Args:
      statement (str): actual human statement at this dialog turn
      next_state_option (tuple) = (possible_statement_of_intent, destination_state_name)
    """
    intent, next_state = next_state_option
    statement = str(statement or '')

    # normalize the match_condition string
    match_condition = 'KEYWORD_LOWER' if match_condition == 'KEYWORD' else match_condition
    match_condition = 'EXACT_NORMALIZE' if match_condition == 'NORMALIZE' else match_condition
    match_condition = 'KEYWORD_EXACT' if match_condition == 'CASE_SENSITIVE_KEYWORD' else match_condition
    next_state = next_state_option[1]

    # preprocessing
    statement, intent = statement.strip(), intent.strip()
    match_condition_tokens = match_condition.split('_')
    if 'LOWER' in match_condition_tokens:
        statement, intent = statement.lower(), intent.lower()
    if 'NORMALIZE' in match_condition_tokens:
        statement, intent = [normalize_wikititle(s) for s in (statement, intent)]
    if 'SLUGIFY' in match_condition_tokens:
        statement, intent = [slugify(s) for s in (statement, intent)]
    if 'SQUASH' in match_condition_tokens:
        statement, intent = [squash_wikititle(s) for s in (statement, intent)]
    if 'TOKENIZE' in match_condition_tokens:
        statement = [tokenize(s) for s in (statement, intent)]
    if 'SIMPLETOKENIZE' in match_condition_tokens or 'RETOKENIZE' in match_condition_tokens:
        statement = [simple_tokenize(s) for s in (statement, intent)]

    # condition matching
    if match_condition.startswith('EXACT') and statement == intent:
        return next_state
    if match_condition.startswith('KEYWORD') and intent in statement:  # FIXME: new match_condition KEYWORD_TOKENIZE
        return next_state
    if match_condition.startswith('FUZZY'):
        """ etl.intents.Intents().predict

        >>> self.fuzzy_intents.predict('joyous people')
        'happy'
        >>> intents.predict('joyous people from Idaho who are conservative')
        'be'
        """
        pass

    return False
